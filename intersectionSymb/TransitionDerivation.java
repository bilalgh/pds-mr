import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableUtils;

public class TransitionDerivation implements Writable {
	
   private String source;
   private String sequence;
   private String end;
   
public TransitionDerivation() {
	super();
	source = new String();
	sequence = new String();
	end = new String();
}

public TransitionDerivation(String inState, String sequence, String outState) {
	super();
	this.source = new String(inState);
	this.sequence = new String(sequence);
	this.end = new String(outState);
}

public TransitionDerivation(TransitionDerivation t) {
	super();
	this.source = new String(t.getSource());
	this.sequence = new String(t.getSequence());
	this.end =  new String(t.getEnd());
}


public TransitionDerivation concat2Transitions(TransitionDerivation t){
	if (this.end.equals(t.getSource())) {
		return new TransitionDerivation(this.getSource(), this.getSequence()+t.getSequence(), t.getEnd());
	}else {
		String s=this.end;
		s = s.replace(t.getSource()+":", "");
		s = s.replace(":"+t.getSource() , "");
		return new TransitionDerivation(this.getSource(), this.getSequence()+t.getSequence(), s);
	}
	
}


@Override
public String toString() {
	return "" + source + " "+ sequence + " " + end	+ "";
}



public String getSource() {
	return source;
}

public void setSource(String source) {
	this.source = source;
}

public String getSequence() {
	return sequence;
}

public void setSequence(String sequence) {
	this.sequence = sequence;
}

public String getEnd() {
	return end;
}

public void setEnd(String end) {
	this.end = end;
}

public void readFields(DataInput in) throws IOException {
	this.source = WritableUtils.readString(in);
	this.sequence = WritableUtils.readString(in);
	this.end = WritableUtils.readString(in);
}

public void write(DataOutput out) throws IOException {
	WritableUtils.writeString(out, this.source);
	WritableUtils.writeString(out, this.sequence);
	WritableUtils.writeString(out, this.end);
	//this.idWFST.write(out);
}
   
}
