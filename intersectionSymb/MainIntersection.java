import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.GenericOptionsParser;

public class MainIntersection {

	
	public static void main(String[] args) throws IOException,
			InterruptedException, ClassNotFoundException {

		System.out.println("*********************************************");
		System.out.println("*           MAIN ENTRY POINT                *");
		System.out.println("*********************************************");

		Configuration conf = new Configuration();
		String[] otherArgs = new GenericOptionsParser(conf, args)
				.getRemainingArgs();
		long startTime = System.currentTimeMillis();

		if (otherArgs.length != 4) {
			System.err
					.println("Usage: recipe <in> <out> <numState> <numSymb> ");
			System.exit(-1);
		}
		
		conf.setInt("numState", Integer.valueOf(otherArgs[2]));
		conf.setInt("numSymb", Integer.valueOf(otherArgs[3]));
		
		// ########################################### First step : Intersection ######################################
		
		// delete output file if exist
		/*
		FileSystem fs = FileSystem.get(conf);
		if (fs.exists(new Path(String.valueOf(otherArgs[1])))) {
			fs.delete(new Path(String.valueOf(otherArgs[1])), true);
		}*/

		Job job1 = Job.getInstance(conf, "Intersection");

		//map
		job1.setJarByClass(MainIntersection.class);
		job1.setMapperClass(MapperIntersection.class);
		//reducer
		//job1.setCombinerClass(ReducerIntersection.class);
		job1.setReducerClass(ReducerIntersection.class);
		job1.setOutputKeyClass(KeyItersection.class);
		job1.setOutputValueClass(TransitionIntersection.class);
		//in & out
		FileInputFormat.setInputPaths(job1, new Path(otherArgs[0]));
		FileOutputFormat.setOutputPath(job1, new Path(otherArgs[1]));

		// Wait for the job to complete and print if the job was successful or not
		job1.waitForCompletion(true);

		if (job1.isSuccessful()) {
			System.out.println("Intersection job was successful");
		} else {

			System.out.println("Intersection job was not successful");
		}
		// ##################################### TIME STATISTICS
		// #####################################
		double executionTime = (System.currentTimeMillis() - startTime) / 1000.0;
		System.out
				.println("########################################################");
		System.out
				.println("#   Hadoop Intersection job take " + executionTime + " sec.");
		System.out
				.println("########################################################");
		System.exit(0);
		
	}

}
