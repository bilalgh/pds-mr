import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

public class ReducerIntersection extends
		Reducer<KeyItersection, TransitionIntersection, Text, Text> {

	List<TransitionIntersection>[] states;

	@SuppressWarnings("unchecked")
	@Override
	protected void setup(Context context) throws IOException,
			InterruptedException {
		int numState = context.getConfiguration().getInt("numState", 0);
		if (numState == 0) {
			System.err
					.println("Exception in ReducerIntersection setup: numState = "
							+ numState);
			System.exit(-1);
		}
		states = new List[numState];
		
		super.setup(context);
	}

	@Override
	protected void reduce(KeyItersection key,
			Iterable<TransitionIntersection> values, Context context)
			throws IOException, InterruptedException {

		Iterator<TransitionIntersection> valuesIt = values.iterator();

		List<TransitionIntersection> listSource;
		List<TransitionIntersection> listEnd;
		TransitionIntersection transition;
		TransitionDerivation transResult;

		String s, e, ee, e1, e2;
		int n;
		// temp var
		TransitionIntersection t1, t2;
		String inSymb = key.getSymb();

		for (int i = 0; i < states.length; i++) {
			states[i] = new ArrayList<TransitionIntersection>();
		}
		
		while (valuesIt.hasNext()) {
			transition = new TransitionIntersection(valuesIt.next());
			n = Integer.valueOf(transition.getInState());
			states[n].add(transition);
		}

		for (int l = 0; l < states.length; l++) {
			for (int m = l + 1; m < states.length; m++) {

				listSource = states[l];
				listEnd = states[m];
				
				
				transResult = null;
				s = l + "," + m;
				loop2: 
				for (int i = 0; i < listSource.size(); i++) {
					for (int j = 0; j < listEnd.size(); j++) {
						if (listSource.get(i).getOutSymbol()
								.equals(listEnd.get(j).getOutSymbol())) {// symbol
																			// contient
																			// in
																			// et
																			// out

							t1 = listSource.get(i);
							t2 = listEnd.get(j);
							// inSymb = t1.getInSymbol();
							e1 = t1.getOutState();
							e2 = t2.getOutState();

							e = e1 + "," + e2;
							ee = e2 + "," + e1;
							if (!e1.equals(e2) && !s.equals(e) && !s.equals(ee)) {// the
																					// same
																					// end

								if (e1.compareTo(e2) < 0) {
									if (transResult == null) {
										transResult = new TransitionDerivation(
												s, inSymb, e1 + "," + e2);
									} else {
										transResult.setEnd(transResult.getEnd()
												+ ":" + e1 + "," + e2);
									}
								} else {
									if (transResult == null) {
										transResult = new TransitionDerivation(
												s, inSymb, e2 + "," + e1);
									} else {
										transResult.setEnd(transResult.getEnd()
												+ ":" + e2 + "," + e1);
									}
								}

							} else {
								if (transResult == null) {
									transResult = new TransitionDerivation(s,
											inSymb, "*");
								} else {
									transResult.setEnd("*");
								}
								break loop2;// FSM observable
							}
						}
					}// End for 1
				}// End for 1

				if (transResult == null) {
					context.write(new Text(" "), new Text(s + " " + inSymb
							+ " #"));
				} else {
					context.write(new Text(" "),
							new Text(transResult.getSource() + " "
									+ transResult.getSequence() + " "
									+ transResult.getEnd()));
				}

			}
		}

	}

	@Override
	protected void cleanup(Context context) throws IOException,
			InterruptedException {

		// context.getCounter("exists", "exists").increment(exist);
		super.cleanup(context);
	}
}
