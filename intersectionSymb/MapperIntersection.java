import java.io.IOException;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

public class MapperIntersection extends
		Mapper<LongWritable, Text, KeyItersection, TransitionIntersection> {

	
	@Override
	protected void map(LongWritable key, Text value, Context context)
			throws IOException, InterruptedException {

		String[] line = value.toString().split(" ");
		TransitionIntersection t = new TransitionIntersection(line[0], line[1],line[2], line[3]);
		
		KeyItersection k = new KeyItersection(line[1]);
		
		context.write(k, new TransitionIntersection(t));

	}
}
