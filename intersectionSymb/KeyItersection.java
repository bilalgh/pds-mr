import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import org.apache.hadoop.io.WritableComparable;
import org.apache.hadoop.io.WritableUtils;


public class KeyItersection implements WritableComparable<KeyItersection> {

	private String symb;
	
	
	public KeyItersection() {
		symb = new String();
	}
	
	public KeyItersection(String symb) {
		this.symb = new String(symb);
	}

	public KeyItersection(KeyItersection k) {
		this.symb = new String(k.symb);
	}

	public void readFields(DataInput in) throws IOException {
		
		this.symb = new String();
		this.symb = WritableUtils.readString(in);
	}

	public void write(DataOutput out) throws IOException {
		
		WritableUtils.writeString(out, this.symb);
			
	}

	
	public String getSymb() {
		return symb;
	}

	public void setSymb(String symb) {
		this.symb = symb;
	}

	
	@Override
	public String toString() {
		return "(" + symb + ")";
	}

	public int compareTo(KeyItersection k) {
		
		return symb.compareTo(k.symb);
	}

}
