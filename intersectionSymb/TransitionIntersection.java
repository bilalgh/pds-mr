import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableUtils;

public class TransitionIntersection implements Writable {
	
   private String inState;
   private String inSymbol;
   private String outSymbol;
   private String outState;
   
public TransitionIntersection() {
	super();
	inState = new String();
	inSymbol = new String();
	outSymbol = new String();
	outState = new String();
}

public TransitionIntersection(String inState, String inSymb,  String outSymb, String outState) {
	super();
	this.inState = new String(inState);
	this.inSymbol = new String(inSymb);
	this.outSymbol = new String(outSymb);
	this.outState = new String(outState);
}

public TransitionIntersection(TransitionIntersection t) {
	super();
	this.inState = new String(t.getInState());
	this.inSymbol = new String(t.getInSymbol());
	this.outSymbol = new String(t.getOutSymbol());
	this.outState =  new String(t.getOutState());
}


/*
public Transition add2Transition(Transition t1,Transition t2){
	
	return new Transition(t1.getInState().add2Etat(t1.getInState(), t2.getInState()),
			t1.getSymbole().add2Symbole(t1.getSymbole(), t2.getSymbole()), 
			t1.getOutState().add2Etat(t1.getOutState(), t2.getOutState()),"T_result");
}
*/

@Override
public String toString() {
	return "" + inState + ","
			+ inSymbol + " " + outSymbol + " " + outState
			+ "";
}


public String getInState() {
	return inState;
}

public void setInState(String inState) {
	this.inState = inState;
}

public String getInSymbol() {
	return inSymbol;
}

public void setInSymbol(String inSymbol) {
	this.inSymbol = inSymbol;
}

public String getOutSymbol() {
	return outSymbol;
}

public void setOutSymbol(String outSymbol) {
	this.outSymbol = outSymbol;
}

public String getOutState() {
	return outState;
}

public void setOutState(String outState) {
	this.outState = outState;
}

public void readFields(DataInput in) throws IOException {
	this.inState = WritableUtils.readString(in);
	this.inSymbol = WritableUtils.readString(in);
	this.outSymbol = WritableUtils.readString(in);
	this.outState = WritableUtils.readString(in);
}

public void write(DataOutput out) throws IOException {
	WritableUtils.writeString(out, this.inState);
	WritableUtils.writeString(out, this.inSymbol);
	WritableUtils.writeString(out, this.outSymbol);
	WritableUtils.writeString(out, this.outState);
	//this.idWFST.write(out);
}
   
}
